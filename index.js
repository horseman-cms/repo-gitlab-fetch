const fetch = require("node-fetch");

function add({
  token,
  baseUrl,
  projectID,
  filePath,
  branch,
  author_email,
  author_name,
  content,
  commit_message,
}) {
  return fetch(`${baseUrl}/api/v4/projects/${projectID}/${filePath}`, {
    method: "post",
    headers: { "Private-Token": token, "CONTENT-TYPE": "application/json" },
    body: JSON.stringify({
      branch,
      author_email,
      author_name,
      content,
      commit_message,
    }),
  });
}

function get({ token, baseUrl, projectID, filePath, branch }) {
  return fetch(
    `${baseUrl}/api/v4/projects/${projectID}/${filePath}/raw?ref=${branch}`,
    {
      headers: { "Private-Token": token, "CONTENT-TYPE": "application/json" },
    }
  );
}

function remove({
  token,
  baseUrl,
  projectID,
  filePath,
  branch,
  author_email,
  author_name,
  commit_message,
}) {
  return fetch(`${baseUrl}/api/v4/projects/${projectID}/${filePath}`, {
    method: "delete",
    headers: { "Private-Token": token, "CONTENT-TYPE": "application/json" },
    body: JSON.stringify({ branch, author_email, author_name, commit_message }),
  });
}

function update({
  token,
  baseUrl,
  projectID,
  filePath,
  branch,
  author_email,
  author_name,
  content,
  commit_message,
}) {
  return fetch(
    `${baseUrl}/api/v4/projects/${projectID}/repository/files/${filePath}`,
    {
      method: "put",
      headers: { "Private-Token": token, "CONTENT-TYPE": "application/json" },
      body: JSON.stringify({
        branch,
        author_email,
        author_name,
        content,
        commit_message,
      }),
    }
  );
}

module.exports = {
  add,
  get,
  remove,
  update,
};
